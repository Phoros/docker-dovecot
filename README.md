# Dovecot in a container 
Small Docker image providing the Dovecot MDA server running on Alpine Linux

# Features
* IMAP support
* ManagedSieve support
* Virtual Mail Folder support
* TLS encryption with strong cipher suites
* Rspamd integration
* Auto-restart on certificate change

This image does not support POP3 or unsecured connections (with or without certificates) and is part of a multi-container setup with dependencies described below.

# Dependencies / Prerequisites
This image depends on external ressources to fully work. Make sure they are available before you run a container.

* MySQL database
* TLS certificates (only certificate and key needed)
* Postfix to receive mails (you're lucky that I provide this image, too!)

### Optional dependencies
This image supports [Rspamd](https://rspamd.com/) for spam detection and filtering. Whenever you move mails to your spam folder or from your spam folder the Rspamd plugin for Dovecot will tag or untag it as spam so you can improve spam detection. Define both Rspamd related environment variables to use it.

# Environment variables
Bold variables indicate that they should be defined or container startup will fail.

| Variable | Type | Purpose |
| -------  | ---- | ------- |
| **TLS_DIRECTORY** | String (unix path) | The directory where TLS certificates are kept without trailing slash. This image tries to find certificates that conform to the Let's Encrypt filename definitions (fullchain.pem). Make sure your certificates are named accordingly or define `TLS_CERT_FILENAME`. |
| CERT_FILENAME | String (filename) | The filename of your certificate file. Default value is `fullchain.pem`. |
| CERT_KEY_FILENAME | String (filename) | The name of your private keyfile. Default value is `privkey.key`. |
| **POSTMASTER_MAIL** | String (mail address) | The mail address defined here is used for bounce mails (storage full, etc.). |
| **SASL_PORT** | Integer [1-65535] | The port for SASL authentication between Dovecot and the MTA. **This port should be kept internal, do not try to publish it!** |
| QUOTA_SERVICE_PORT | Integer [1-65535] | The port for the quota service. Postfix can then check the available quota to ensure the mail does not hit any quota limits or it can reject mails that won't fit anymore. |
| MAIL_SUBADDRESS_DELIMITER | Char | The character that splits the local-part and the subaddress of a mail address. This character must be the same as the one Postfix is using. Default value is ̀̀̀́́́́́́́́́́́́́́`-`. |
| RSPAMD_HOST | String (hostname or IP address) | The hostname of the Rspamd instance for the spam plugin. |
| RSPAMD_PORT | Integer [1-65535] | The port of the Rspamd instance for the spam plugin. |
| RSPAMD_PASSWORD | String | The enable password of the Rspamd instance. Do not use the hashed representation. |
| INTERNAL_NETWORK | String (IP subnet) | The binding network for all internal interfaces of Dovecot. These interfaces should be opened inside the container only. When no internal network is defined the image binds them to 0.0.0.0 which effectively means world-readable. As long as no internal ports are published this is safe enough to run. |

# Database support
This image only supports MySQL-based databases. You need to create a file and mount it into the container to deliver credentials to Dovecot (see *Pre-defined directories*). An example file is located [here](/sql/dovecot-sql.conf.ext). You need to change the connect parameters like `host` and `password`. The used MySQL scheme for this example can be found [here](/sql/vmail.sql) and can be directly imported into MySQL.

# Pre-defined directories
This image has a special directory structure where config files and user given files are located. If you mount your own files into this image, make sure you mount them as read-only. You can find config examples at the Dovecot GitHub page [here](https://github.com/dovecot/core/tree/master/doc/example-config). Bold entries are necessary.

- /etc/dovecot/ - All config files of Dovecot reside here. You can mount your own files for special configuration which is not covered by this image.
  - **/etc/dovecot/dovecot-sql.conf.ext** - The MySQL database configuration for Dovecot.
- /var/mail/ - Mails are stored here. This folder is managed by Dovecot. Do not change or modify file permissions or files on your own. The image takes care of them.
  - /var/mail/virtual/ - Virtual folder files should be placed here. Mount them in by using a volume or a host folder and change file and folder permission to 0750 and the owner to 5000:5000. **Do not mount them in as read-only!**
  - /var/mail/sieve/ - Sieve related files should be placed here. Like the virtual folder files the permissions need to be set to 0750 and the owner to 5000:5000. **Also do not mount them in read-only!***
    - /var/mail/sieve/sieve-before/ - Global sieve files should be placed here. These files are executed *before* user sieve files.
    - /var/mail/sieve/sieve-after/ - Global sieve files should be placed here. These files are executed *after* user sieve files
    - /var/mail/<domain>/<user>/sieve/sieve.d/ - User sieve files are stored here. These are usually managed by the mail client.
    - /var/mail/<domain>/<user>/sieve/sieve-before.d/ - User sieve files are stored here. These files are executed *before* any user sieve files in `sieve.d`.
    - /var/mail/<domain>/<user>/sieve/sieve-after.d/ - User sieve files are stored here. These files are executed *after* any user sieve files in `sieve.d`.

# Persistent directories
Use a persistent volume or host path to make sure these files will survive a container removal.

- /var/mail/ - Mail storage. Make sure you really persist this directory or you will lose mails. This directory includes sieve scripts and virtual mail folders.

# How to use
1. Create a volume to persist mails: `docker volume create dovecot-mails`
2. Create a network to restrict access and isolate Dovecot and Postfix from other containers: `docker network create --attachable --internal --subnet 172.18.0.0/16 mail`
3. Create the Dovecot container `docker create --name dovecot --hostname imap.example.com --publish 993:993 --publish 4190:4190 --env TLS_DIRECTORY=/etc/ssl/ --env POSTMASTER_MAIL=postmaster@example.com --env SASL_PORT=12345 --env INTERNAL_NETWORK=172.18.0.0 --mount type=bind,source="$(pwd)"/keys/,destination=/etc/ssl/ --mount type=volume,source=dovecot-mails,destination=/var/mail/ registry.gitlab.com/phoros-docker/dovecot:latest`
4. Attach the container to the mail Docker network: `docker network connect mail dovecot`
5. Start the container: `docker start dovecot`

This example uses port 12345 as the SASL port for communication between Dovecot and the MTA. You can choose any port you like. Define the INTERNAL_NETWORK environment variable when you know the subnet this container is running in (Docker uses subnets like 172.*.0.0).
